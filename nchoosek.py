# n-choose-k
# recursive, factorial, and multiplicative

def nck_recursive(n, k):
	if k == 0 or k == n:
		return 1
	else:
		return nck_recursive(n-1, k) + nck_recursive(n-1, k-1)

def fact(n):
	if n <= 1:
		return 1
	else:
		return n*fact(n-1)

def nck_factorial(n, k):
	return fact(n) / (fact(k) * fact(n-k))

def nck_multiplicative(n, k):
	result = 1
	for i in range(1, k+1):
		result = result * (n-(k-i))/i
	return result

def pascal_demo(n):
	alllists = []
	ns = range(n)
	for n in ns:
		nlist = []
		for k in range(n+1):
			nlist.append(nck_multiplicative(n, k))
		nlist = ' '.join(map(str, nlist))
		alllists.append(nlist)
	return alllists

def center(strings):
	maxlistlen = len(max(strings, key=len))
	for i, s in enumerate(strings):
		diff = maxlistlen - len(s)
		pad = ' '*(diff/2)
		yield str(i)+' -> '+pad+s

if __name__ == '__main__':
	ps = pascal_demo(15)
	for c in center(ps):
		print c